package ru.tsc.pavlov.tm;

import ru.tsc.pavlov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        showWelcome();
        parseArgs(args);
        process();
    }

    public static void parseArg(final String arg) {
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
        if (TerminalConst.HELP.equals(arg)) showHelp();
        if (TerminalConst.EXIT.equals(arg)) exit();
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        exit();
    }

    public static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Ruslan Pavlov");
        System.out.println("E-MAIL: o000.ruslan@yandex.ru");
        exit();
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
        exit();
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println("about - display developer info");
        System.out.println("version - display program info");
        System.out.println("help - display list of commands");
        exit();
    }

    public static void process() {
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            parseArg(command);
        }
    }

    public static void exit() {
        System.exit(0);
    }

}
